# http-cors-header-interceptor

## Install dependencies

```npm install```

## Run

```node cors.js```

## How it works

When accessing an API without Orign Allow Header e.g.```http://localhost:8000/chat``` access the api like this ```http://localhost:8888/http://localhost:8000/chat```
